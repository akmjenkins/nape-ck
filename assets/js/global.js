//load all required scripts
;(function(context) {
	
	var tests;
	var preventOverScroll;

	if(context) {
		preventOverScroll = context.preventOverScroll;
		tests = context.tests;
	} else {
		preventOverScroll = require('./preventOverScroll.js');
		tests = require('./tests.js');
	}

	$(document)
		.on('updateTemplate.lazyYT',function() {
			
			$('div.lazyyt')
				.filter(function() {
					return !$(this).hasClass('.lazyYT-container');
				})
				.each(function() { 
					$(this).lazyYT(); 
				});	
				
		}).trigger('updateTemplate.lazyYT');
		

	preventOverScroll($('div.nav')[0]);

	$('.fader').each(function() {

		var 
			
			slickEl,
			el = $(this),
			methods = {
				
				getElementWithSrcData: function(el) {
					return el.data('src') !== undefined ? el : el.find('.bgel').filter(function() { return $(this).data('src') !== undefined });
				},
				
				loadImageForElementAtIndex: function(i) {
					var 
						self = this;
						element = $('.fader-item',el).eq(i),
						sourceElement = this.getElementWithSrcData(element),
						source = sourceElement.data('src');
					
					if(!element.hasClass('loading') && !element.hasClass('loaded')) {
						element.addClass('loading');
						this
							.loadImage(source)
							.then(function() {
								sourceElement
									.add(self.getElementWithSrcData(element.siblings()).filter(function() {
										return $(this).data('src') === source;
									}))
									.css({backgroundImage: 'url('+source+')' })
									.addClass('loaded');
							});
					}
					
				},
				
				loadImage: function(src) {
					var dfd = $.Deferred();
					
					$('<img/>')
						.on('load',function() {
							dfd.resolve();
						})
						.attr('src',src);
				
					return dfd.promise();
				}

			};

		el.slick({
			dots:true,
			appendDots:$('.fader-nav',el.parent()),
			appendArrows:$('.fader-controls',el.parent()),
			prevArrow: '<button class="prev"/>',
			nextArrow: '<button class="next"/>',
			draggable:false,
			swipe:true,
			touchMove:true,
			autoplay:true,
			autoplaySpeed: 5000,
			pauseOnHover: false,
			fade:!tests.touch()
		});
		
		el.on('beforeChange',function(slick,e,i) {
			methods.loadImageForElementAtIndex(!tests.touch() ? i-1 : i);
		});
		
		methods.loadImageForElementAtIndex(tests.touch() ? 1 : 0);

	});

}(typeof ns !== 'undefined' ? window[ns] : undefined));