<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">History of NAPE</h1>
					<span class="subtitle">How it all started</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">

						<div class="grid nopad history-grid">
						
							<a class="history-item mpopup" id="history-item-1" data-src="#history-item-1" data-gallery="history-items" data-type="inline" data-main-class='[".my-mfp-zoom-in"]'>
								<div class="history-item-content">
									<span class="year">1936</span>
									<span class="description">
										The Civil Service Association is formed by a group of senior civil servants...
									</span><!-- .description -->								
									<span class="description-full">
										The Civil Service Association is formed by a group of senior civil servants 
										class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
										himenaeos. Aenean placerat aliquet felis, nec ultrices nibh finibus eget. 
										Maecenas aliquet odio vel velit sagittis, in tincidunt purus pulvinar. Mauris sem leo, 
										lacinia sit amet ipsum ut, dapibus scelerisque sapien.
									</span><!-- .description-full -->
								</div><!-- .history-item-content -->
							</a><!-- .history-item -->
							
							<a class="history-item mpopup" id="history-item-1" data-src="#history-item-1" data-gallery="history-items" data-type="inline" data-main-class='[".my-mfp-zoom-in"]'>
								<div class="history-item-content">
									<span class="year">1937</span>
									<span class="description">
										Association membership grows to 738 members.
									</span><!-- .description -->								
									<span class="description-full">
										Association membership grows to 738 members.
										class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
										himenaeos. Aenean placerat aliquet felis, nec ultrices nibh finibus eget. 
										Maecenas aliquet odio vel velit sagittis, in tincidunt purus pulvinar. Mauris sem leo, 
										lacinia sit amet ipsum ut, dapibus scelerisque sapien.
									</span><!-- .description-full -->
								</div><!-- .history-item-content -->
							</a><!-- .history-item -->
							
							<a class="history-item mpopup" id="history-item-3" data-src="#history-item-3" data-gallery="history-items" data-type="inline">
								<div class="history-item-content">
									<span class="year">1940-51</span>
									<span class="description">
										The Association gains increases in the war bonus and the cost of living bonus for civil servants...
									</span><!-- .description -->								
									<span class="description-full">
										The Association gains increases in the war bonus and the cost of living bonus for civil servants...
										class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
										himenaeos. Aenean placerat aliquet felis, nec ultrices nibh finibus eget. 
										Maecenas aliquet odio vel velit sagittis, in tincidunt purus pulvinar. Mauris sem leo, 
										lacinia sit amet ipsum ut, dapibus scelerisque sapien.
									</span><!-- .description-full -->
								</div><!-- .history-item-content -->
							</a><!-- .history-item -->
							
							<a class="history-item mpopup" id="history-item-4" data-src="#history-item-4" data-gallery="history-items" data-type="inline">
								<div class="history-item-content">
									<span class="year">1952</span>
									<span class="description">
										The Civil Service Association is formed by a group of senior civil servants...
									</span><!-- .description -->								
									<span class="description-full">
										The Civil Service Association is formed by a group of senior civil servants...
										class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
										himenaeos. Aenean placerat aliquet felis, nec ultrices nibh finibus eget. 
										Maecenas aliquet odio vel velit sagittis, in tincidunt purus pulvinar. Mauris sem leo, 
										lacinia sit amet ipsum ut, dapibus scelerisque sapien.
									</span><!-- .description-full -->
								</div><!-- .history-item-content -->
							</a><!-- .history-item -->
							
							<a class="history-item mpopup" id="history-item-5" data-src="#history-item-5" data-gallery="history-items" data-type="inline">
								<div class="history-item-content">
									<span class="year">1953</span>
									<span class="description">
										Association membership grows to 738 members.
									</span><!-- .description -->								
									<span class="description-full">
										Association membership grows to 738 members.
										class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
										himenaeos. Aenean placerat aliquet felis, nec ultrices nibh finibus eget. 
										Maecenas aliquet odio vel velit sagittis, in tincidunt purus pulvinar. Mauris sem leo, 
										lacinia sit amet ipsum ut, dapibus scelerisque sapien.
									</span><!-- .description-full -->
								</div><!-- .history-item-content -->
							</a><!-- .history-item -->
							
							<a class="history-item mpopup" id="history-item-6" data-src="#history-item-6" data-gallery="history-items" data-type="inline">
								<div class="history-item-content">
									<span class="year">1954-56</span>
									<span class="description">
										The Association gains increases in the war bonus and the cost of living bonus for civil servants...
									</span><!-- .description -->								
									<span class="description-full">
										The Association gains increases in the war bonus and the cost of living bonus for civil servants...
										class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
										himenaeos. Aenean placerat aliquet felis, nec ultrices nibh finibus eget. 
										Maecenas aliquet odio vel velit sagittis, in tincidunt purus pulvinar. Mauris sem leo, 
										lacinia sit amet ipsum ut, dapibus scelerisque sapien.
									</span><!-- .description-full -->
								</div><!-- .history-item-content -->
							</a><!-- .history-item -->
							
						</div><!-- .history-grid -->
						
						<button class="t-fa-abs fa-chevron-down load-more-arrow">Load More</button>
					
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							
							<?php include('inc/i-mod-the-latest.php'); ?>
							
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>