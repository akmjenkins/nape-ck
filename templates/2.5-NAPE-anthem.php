<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">NAPE Anthem</h1>
					<span class="subtitle">The official anthem of our union</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/125036017&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
						
						<br />
						<br />
						
						<div class="lazyyt" data-youtube-id="NYxkldkO5GY" data-ratio="20:13"></div>
						
						<br />
						<br />
						
						<div class="article-body">
						
							<p>
								Oh, let us always remember our story <br />
								And how we have grown, <br />
								For there was a day that was darker by far <br />
								When workers fought battles alone. <br />
								(When workers fought battles alone).
							</p>
								 
							<p>
								But now we draw strength from each other <br />
								The source of our hope and our pride <br />
								And the ties that will hold us together as one <br />
								Nobody shall break or divide. <br />
								(Nobody shall break or divide). <br />
							</p>
								 
							<p>								 
								For we know the honour of labour <br />
								The worth of the gifts that we share, <br />
								And we seek the good of our neighbour <br />
								Who employs or is placed in our care. <br />
							</p>
								 
							<p>
								So sing praise my brothers and sisters <br />
								To NAPE who will keep us secure. <br />
								All thanks to NAPE who unites us all <br />
								and long may our union endure. <br />
								(And long my our union endure). 
							</p>
								 
							<p>
								All thanks to NAPE who unites us all <br />
								and long may our union endure.
							</p>
							
							<em>&mdash; Jed Blackmore</em>
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>