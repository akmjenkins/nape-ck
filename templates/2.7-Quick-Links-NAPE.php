<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">Quick Links</h1>
					<span class="subtitle">Helpful Links for Members</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<div class="article-body">
						
							<p>
								<a href="#" rel="external">Government of Newfoundland &amp; Labrador</a> <br />
								<a href="#" rel="external">Public Service Commission</a> <br />
								<a href="#" rel="external">Workplace, Health, Safety and Compensation Commission</a> <br />
								<a href="#" rel="external">NL Federation of Labour</a> <br />
								<a href="#" rel="external">National Union of Public and General Employees</a> <br />
							</p>
						
							<p>
								<small>
									Disclaimer: The information on these external sites are provided for information only. NAPE assumes no responsibility or liability for any damages, direct, indirect or consequential that may result from any error or omission or from the use of any information on any site to which this site may be linked.
								</small>
							</p>
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							
							<?php include('inc/i-mod-the-latest.php'); ?>
							
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>