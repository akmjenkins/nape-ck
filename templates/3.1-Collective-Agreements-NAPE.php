<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">Collective Agreements</h1>
					<span class="subtitle">Find your Collective Agreement</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<div class="article-body">
						
							<p>
								NAPE has negotiated over 100 collective agreements on behalf of over 25,000 members in Newfoundland and Labrador. 
								To read or download an agreement, please select one. Agreements are in PDF format.
							</p>
							
							<p>
								<small>
									Disclaimer: If there is a discrepancy between the online version and the official, hard-copy, signed version, the latter shall have precedence.
								</small>
							</p>
						
						</div><!-- .article-body -->
						
						<div class="single-form-wrap">
							<form action="" class="single-form">
								<div class="fieldset">
									<input type="text" placeholder="Search Collective Agreements">
									<button type="submit" class="fa-search">&nbsp;</button>
								</div>
							</form>
							<span class="single-form-meta">
								108
								<span class="small">Agreements</span>
							</span><!-- .form-meta -->
						</div><!-- .single-form-wrap -->
						
						<br />
						<br />
						
						<div class="paginated-items-container">
							<div class="paginated-items-header">
							
								<div class="selector with-arrow">
									<select name="sort">
										<option value="" data-tag="Sort By Most Recent">Most Recent</option>
										<option value="" data-tag="Sort By Most Viewed">Most Viewed</option>
										<option value="" data-tag="Sort By Title">Title</option>
									</select>
									<span class="value">&nbsp;</span>
								</div><!-- .selector -->
								
							</div><!-- .paginated-items-header -->
							
							<div class="paginated-items">

								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">A Better Living Home Care</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
								
								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">Angels Touch Home Care (2014)</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
								
								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">A Better Living Home Care</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
								
								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">A Better Living Home Care</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
								
								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">A Better Living Home Care</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
								
								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">Angels Touch Home Care (2014)</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
								
								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">A Better Living Home Care</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
								
								<div class="paginated-item agreement-item">
								
									<span class="tag button fill">Agreements</span>
								
									<div class="agreement-item-content">
										<span class="title">A Better Living Home Care</span>
										<time datetime="2014-01-01">Posted: July 15, 2013</time>
									</div><!-- .content -->
									
									<div class="actions">
										<div class="selector with-arrow">
											<select>
												<option value="">Choose Action</option>
												<option value="">View</option>
												<option value="">Print</option>
												<option value="">Download</option>
											</select>
											<span class="value">&nbsp;</span>
										</div>
									</div><!-- .actions -->
								</div><!-- .paginated-item -->
							
							</div><!-- .paginated-items -->
							
							<div class="paginated-items-footer">
							
								<div class="arrow-controls">
									<!-- these can also be "a" tags -->
									<button class="prev">Prev</button>
									<button class="next">Next</button>
								</div><!-- .arrow-controls -->
								
								<div class="count">8 of 108</div>
							
							</div><!-- .search-result-footer -->
							
						</div><!-- .paginated-items-container -->
						
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>