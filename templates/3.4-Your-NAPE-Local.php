<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="grey-bg">
		<div class="sw">
	
			<div class="inline-search">
			
				<div class="inline-search-form">
					
					<div class="hgroup">
						<h3 class="title">Search for your local</h3>
						<span class="subtitle">Type your question, keyword or phrase into our search box to quickly find what you are looking for.</span>
					</div><!-- .hgroup -->
					
					<form action="/" class="single-form">
						<span class="count">15</span>
						<div class="fieldset">
							<input type="text" name="s" placeholder="Search by Name or your NAPE Local No.">
							<button type="submit" class="fa-search">&nbsp;</button>
						</div><!-- .fieldset -->
					</form>
					
				</div><!-- .inline-search-form -->
			
			</div><!-- .inline-search -->
	
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="grid eqh grid-tagged-items auto-collapse">
			
				<div class="col col-3">
					
						<a class="item grid-item-with-tag clear" href="#">
						
							<span class="item-tag item-tag-text">Local 1403</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">24</span> Mar
							</time>
							
							<div class="hgroup">
								<h5 class="title">St. John's Youth Scholarship Fund has been announced.</h5>
							</div><!-- .hgroup -->
							
							<p>
								The executive board is pleased to announce that this year there will be two scholarships awarded for $2,500.00 each.
							</p>
							
							<span class="read-more-tag">Read More</span>
							
						</a>
					
				</div><!-- .col -->
				
				<div class="col col-3">
					
						<a class="item grid-item-with-tag clear" href="#">
						
							<span class="item-tag item-tag-text">Local 3401</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">17</span> Mar
							</time>
							
							<div class="hgroup">
								<h5 class="title">Volunteers required for the Leadership Training Course.</h5>
							</div><!-- .hgroup -->
							
							<p>
								This is a six-month leadership training program designed to develop an organization's current and future leaders. 
							</p>
							
							<span class="read-more-tag">Read More</span>
							
						</a>
					
				</div><!-- .col -->

				<div class="col col-3">
					
						<a class="item grid-item-with-tag clear" href="#">
						
							<span class="item-tag item-tag-text">Local 7003</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">12</span> Mar
							</time>
							
							<div class="hgroup">
								<h5 class="title">General Membership Meeting</h5>
								<span class="subtitle"><em>May 25, 2014</em></span>
							</div><!-- .hgroup -->

							<strong>Event:</strong>
							General Membership Meeting
							
							<br />
							<br />
						
							<strong>Date:</strong>
							Sunday, May 25th/2014 Time: 10:00 a.m.
							
							<span class="read-more-tag">Read More</span>
							
						</a>
					
				</div><!-- .col -->

				<div class="col col-3">
					
						<a class="item grid-item-with-tag clear" href="#">
						
							<span class="item-tag item-tag-text">Local 5205</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">14</span> Feb
							</time>
							
							<div class="hgroup">
								<h5 class="title">National Nursing Week</h5>
								<span class="subtitle"><em>May 12 - 18, 2014</em></span>
							</div><!-- .hgroup -->

							<p>
								The Theme &mdash; "Nursing: A Leading Force For Change" recognizes the vital role nurses play.
							</p>
							
							<span class="read-more-tag">Read More</span>
							
						</a><!-- .item -->
					
				</div><!-- .col -->
				
				<div class="col col-3">
					
						<a class="item grid-item-with-tag clear" href="#">
						
							<span class="item-tag item-tag-text">Local 5205</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">14</span> Feb
							</time>
							
							<div class="hgroup">
								<h5 class="title">National Nursing Week</h5>
								<span class="subtitle"><em>May 12 - 18, 2014</em></span>
							</div><!-- .hgroup -->

							<p>
								The Theme &mdash; "Nursing: A Leading Force For Change" recognizes the vital role nurses play.
							</p>
							
							<span class="read-more-tag">Read More</span>
							
						</a><!-- .item -->
					
				</div><!-- .col -->
				
				<div class="col col-3">
					
						<a class="item grid-item-with-tag clear" href="#">
						
							<span class="item-tag item-tag-text">Local 7003</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">14</span> Feb
							</time>
							
							<div class="hgroup">
								<h5 class="title">Local 7003 Providing Solid Training Opportunities Will Help Fill Job Vacancies</h5>
							</div><!-- .hgroup -->

							<p>
								Over the last six months, Local 7003 has brought our concerns about the...
							</p>
							
							<span class="read-more-tag">Read More</span>
							
						</a><!-- .item -->
					
				</div><!-- .col -->
				
			</div><!-- .grid -->
			
			<div class="arrow-controls centered">
				<!-- these can also be "a" tags -->
				<button class="prev">Prev</button>
				<button class="next">Next</button>
			</div><!-- .arrow-controls -->
		
		</div><!-- .sw -->
	</section>
	
	<hr class="sw" />	
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>