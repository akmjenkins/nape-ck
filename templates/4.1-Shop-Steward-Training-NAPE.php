<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
					
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">President's Bio</h1>
					<span class="subtitle">Carol Furlong</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<div class="article-body">
						
							<div class="lazybg featured-img">
								<img src="http://dummyimage.com/800x320/ccc/fff.png" alt="nape president carol furlong">
							</div><!-- .lazybg -->
							
							<p>
								The Steward is the visible presence of the union in the workplace as the union officer who works
								with and interacts with the members at the workplace and represents them in a specific work area.
							</p>
								 
							<p>
								The Steward enforces the collective agreement and protects the rights members have acquired
								through negotiations and other union actions. Stewards are elected or appointed within the Local.
								Shop stewards are members’ first line of defense, responsible for enforcing the contract and
								handling grievances, and for proactively ensuring worksite safety, mobilizing members for the
								union’s political campaigns, orienting new members to the worksite and the union, and more.
							</p>
								 
							<p>
								The Steward acts as the liaison between the Local Executive and the membership. It is their job to
								make sure the members they represent at the worksite know what the union and Local are doing.
							</p>
								 
							<p>
								The Steward is a key person in the union and it is within their power to ensure their Local is strong,
								representative and successful in protecting membership rights.
							</p>
								 
							<p>
								If you are interested in becoming a Steward, contact your Local Executive or email
								<a href="#">kdunne@nape.nf.ca</a>.
							</p>
								 
							<p>
								NAPE is continuing to offer the newly revised two-day Shop Steward training seminars. This seminar
								is meant for all Shop Stewards (previously trained, untrained or seasoned Stewards). All those who
								have not already attended this updated seminar are encouraged to attend.
							</p>
								 
							<p>
								Seminars will be scheduled for each region and Shop Stewards will be notified once dates are set.
								NAPE’s goal is to provide this training to all of our Shop Stewards across the province.
							</p>
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<h3 class="section-title">Upcoming Shop Steward Seminars</h3>
			<hr />
			
			<div class="grid collapse-950">
				<div class="col col-3-5">
					<div class="item">
					
						<div class="grid eqh collapse-650">
							<div class="col col-2">
								<a class="item grid-item-with-tag clear" href="#">
								
									<span class="item-tag tag-event">Event</span>
									
									<time datetime="2014-03-24" class="i">
										<span class="day">12</span> Feb
									</time>									
									
									<div class="hgroup">
										<h4 class="title">Seminar - Region 5</h4>
									</div><!-- .hgroup -->
									<div class="article-body">
										<p>
											A Shop Steward Seminar has been scheduled for 
											Region 5. This Seminar is meant for any 
											Shop Steward who has not received the 
											two-day training course.
										</p>
										
										<p>
											<strong>Date:</strong> <br />
											December 8 and 9, 2014 (Registration at 8:30 a.m.)
										</p>
										
										<p>
											<strong>Place:</strong> <br />
											Bay Roberts (Ground Search and Rescue)
										</p>
										
										<p>
											<strong>Time:</strong> <br />
											Monday, December 8 – 9:00 – 5:00 (Lunch will be provided) <br />
											Tuesday, December 9 – 9:00 – 5:00 (Lunch will be provided)
										</p>
									</div><!-- .article-body -->
									
									<span class="read-more-tag">Read More</span>
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<a class="item grid-item-with-tag clear" href="#">
								
									<span class="item-tag tag-event">Event</span>
									
									<time datetime="2014-03-24" class="i">
										<span class="day">15</span> Feb
									</time>									
									
									<div class="hgroup">
										<h4 class="title">Seminar - Region 5</h4>
									</div><!-- .hgroup -->
									<div class="article-body">
										<p>
											A Shop Steward Seminar has been scheduled for Region 5
										</p>
										
										<p>
											<strong>Date:</strong> <br />
											December 8 and 9, 2014 (Registration at 8:30 a.m.)
										</p>
										
										<p>
											<strong>Place:</strong> <br />
											Bay Roberts (Ground Search and Rescue)
										</p>
										
										<p>
											<strong>Time:</strong> <br />
											Monday, December 8 – 9:00 – 5:00 (Lunch will be provided) <br />
											Tuesday, December 9 – 9:00 – 5:00 (Lunch will be provided)
										</p>
									</div><!-- .article-body -->
									
									<span class="read-more-tag">Read More</span>
								</a><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2-5">
					<div class="item">
					
						<div class="hgroup">
							<h4 class="title">Do you want to attend?</h4>
							<span class="subtitle">If you wish to attend this Seminar, you must contact the Accounting Department:</span>
						</div><!-- .hgroup -->
						
						<p>
						
							<strong>Phone: </strong> 754-7000 <br />
							<strong>Toll Free: </strong> 1-800-563-4442 <br />
						
						</p>
						
						<br />
						<br />
						
						<a href="#" class="inline">bevans@nape.nf.ca</a> <br />
						<a href="#" class="inline">ebursey@nape.nf.ca</a> <br />
						<a href="#" class="inline">rconnors@nape.nf.ca</a> <br />
						
						<br />
						
						<p>
							Employers require two (2) weeks’ notice for time off, so please register as soon as possible.
						</p>
						
						<br />
						
						<p>
							PLEASE BRING YOUR CONTRACT WITH YOU TO THE SEMINAR!
						</p>
						
					</div><!-- .item -->
				</div><!-- .col.col-2-5 -->
			</div><!-- .grid -->
			
			
		
		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>