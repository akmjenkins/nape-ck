<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
					
			<div class="hgroup article-head">
				<h1 class="title">Gallery</h1>
				<span class="subtitle">Vivamus pulvinar tortor eget nibh gravida</span>
			</div><!-- .hgroup -->
			
			<div class="grid media-grid eqh fill">
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<div class="item">
					
						<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="16:9"></div>
						
						<time datetime="2014-02-13">February 13, 2014</time>
						<span class="title">Fusce nec Nibh Scelerisque Neque Gravida Sodales Fusce nec Nibh Scelerisque Neque Gravida Sodales</span>
						<a href="#" class="button fill">View</a>
					
					</div><!-- .item -->
				</div>
			</div><!-- .grid -->


		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>