<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup article-head">
					<h1 class="title">Publications</h1>
					<span class="subtitle">NAPE Publications</span>
				</div><!-- .hgroup -->
			
				<div class="main-body">
					<div class="content">
					
						<div class="article-body">
						
							<p>
								NAPE publishes a regular newsletter called the Communicator for members and the general public. 
								Previous editions are available online here. Other NAPE publications will also be made available on this page.
							</p>
							
							<h3>NAPE Communicator</h3>
							
							<p>
								
								<a href="#">NAPE_Comm 2013_Web</a> <br />
								<a href="#">NAPE_Communicator_2012 (Web)</a> <br />
								<a href="#">NAPE Communicator February 2011</a> <br />
								
							</p>
							
							<h3>Other Publications</h3>
							
							<p>
								<a href="#">Breach of the Peace Hearings Report</a> <br />
								<a href="#">Human Rights Day 2011 Poster</a>
							</p>
							
							<br />
							
							<form action="" class="body-form">
								<fieldset>
								
									<input type="text" name="name" placeholder="Name">
									<input type="email" name="name" placeholder="Email">
								
									<button type="submit" class="button fill">Subscribe</button>
								</fieldset>
							</form>
						
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					<aside class="sidebar">
					
						<div class="mod">
							<?php include('inc/i-mod-in-this-section.php'); ?>
						</div><!-- .mod -->
						
						<div class="mod">
							<?php include('inc/i-mod-the-latest.php'); ?>
						</div><!-- .mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>