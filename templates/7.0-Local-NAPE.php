<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="local-header">
		<div class="sw">
			<div class="local-title">Local 7104</div>
			
			<div class="local-description">
				NAPE Local 7104 unites over 2000 General Service workers in the St. John’s area.
			</div><!-- .local-description -->
			
			<div class="local-phone t-fa">
				<div class="local-phone-numbers">
					<span class="block">709-570-2475</span>
					<span class="block">709-682-1548</span>
				</div>
			</div><!-- .local-phone -->
		</div><!-- .sw -->
	</div><!-- .local-header -->

	<div class="big-fader fader local-fader">
		<div class="fader-item">
		
			<div class="fader-item-content sw full">
				
				<a href="#" class="fader-item-content-image bgel" data-src="../assets/dist/images/temp/hero/hero-1.jpg">&nbsp;</a>
				<div class="fader-item-content-title">
					<span class="title">Local 7104 votes on upcoming tenative agreement for public sector workers.</span>
					<span class="subtitle">Lorem ipsum dipsum mipsum ahea dokieo</span>
					<a href="#" class="button">Read More</a>
				</div>
			
			</div><!-- .fader-item-content -->
		
		</div><!-- .fader-item -->
		<div class="fader-item">
		
			<div class="fader-item-content sw full">
				
				<a href="#" class="fader-item-content-image bgel" data-src="../assets/dist/images/temp/hero/hero-2.jpg">&nbsp;</a>
				<div class="fader-item-content-title">
					<span class="title">Local 7104 votes on upcoming tenative agreement</span>
					<span class="subtitle">Lorem ipsum dipsum mipsum ahea dokieo</span>
					<a href="#" class="button">Read More</a>
				</div>
			
			</div><!-- .fader-item-content -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
	<div class="fader-control-wrap sw">
		<div class="arrow-controls fader-controls"></div>
	</div><!-- .fader-control-wrap -->
	
</div><!-- .hero -->

<div class="body">

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
		
		</div><!-- .sw -->
	</section>

	<section>
		<div class="sw">
		
			<div class="news-items">
			
				<div class="grid collapse-1000">
				
					<div class="col-3 col">
						<div class="item">
							
							<h2>Upcoming Events</h2>
							
							<div class="news-item">
								
								<div class="news-item-title">
								
									<time class="i" datetime="2014-03-24">
										<span class="day">24</span>
										Mar
									</time><!-- .i -->
								
									<div class="h5-style">
										Local 5205 Monthly Executive Meeting
									</div>
								
								</div><!-- .pmessage-title -->
								
								<div class="article-body">
								
									<p>
										Proposed Updated Local By-laws &amp; Agreements - To be voted upon 24-April-14.
									</p>
								
								</div><!-- .article-body -->
							
							</div><!-- .news-item -->

							
							<div class="news-item">
								
								<div class="news-item-title">
								
									<time class="i" datetime="2014-06-07">
										<span class="day">07</span>
										Jun
									</time><!-- .i -->
								
									<div class="h5-style">
										NAPE/NLFL Labatt Boycott Launch
									</div>
								
								</div><!-- .news-item-title -->
								
								<a href="#" class="button fill">Read Transcript</a>
								<a href="#" class="button fill">Past Messages</a>
							
							</div><!-- .news-item -->
							
							<div class="news-item">
								
								<div class="news-item-title">
								
									<time class="i" datetime="2014-04-29">
										<span class="day">29</span>
										Apr
									</time><!-- .i -->
								
									<div class="h5-style">
										Have Province? Have People?
									</div>
								
								</div><!-- .news-item-title -->
								
								<a href="#" class="button fill">Read Transcript</a>
								<a href="#" class="button fill">Past Messages</a>
							
							</div><!-- .news-item -->
							
							<div class="news-items-controls">
							
								<div class="arrow-controls">
								
									<!-- these can also be "a" tags -->
									<button class="prev">Prev</button>
									<button class="next">Next</button>
									
								</div><!-- .controls -->
								
								<a href="#" class="button fill">View All</a>
							</div><!-- .news-items-controls -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-2-3 col">
						<div class="item">
							
							<div class="news-item-content">
							
								<div class="news-item-content-body article-body">
								
									<h5>Description</h5>
									
									<p>
										<em>Proposed Updated Local By-laws & Agreements - To be voted upon 24-April-14.</em>
									</p>
									
									<p>
										According to NAPE President Carol Furlong, the
										tentative agreement is based on the template
										agreement for public sector workers. There are also
										additional improvements in monetary and other
										contract language. Furlong stated, “NAPE was pleased
										that all concessions were removed by the employer in
										order to reach a deal”.
									</p>
								
								</div><!-- .content -->
								
								<div class="news-item-meta">
								
									<h5>Location</h5>
									
									<address>
										Health Sciences Centre <br />
										300 Prince Phillip Drive, St. John's <br />
										Conference Room B
									</address>
									<br />
									<a href="#" class="button fill">View Map</a>
									
									<br />
									<br />
									<br />
									
									<h5>Date &amp; Time</h5>
									
									<p>
										Monday, December 8 &mdash; 9:00 &mdash; 5:00 <br />
										Tuesday, December 9 &mdash; 9:00 &mdash; 5:00 <br />
									</p>
								
								</div><!-- .meta -->
							
							</div><!-- .news-item-content -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .news-items -->


			
		</div><!-- .sw -->
	</section>

	<hr class="sw" />	
	
	<section>
		<div class="sw">
		
			<h3>Your Local Executive</h3>
		
			<div class="member-grid">
			
				<div class="member">
					<span class="member-name">John Wells</span>
					<span class="member-title">President</span>
					
					<span class="member-info phone">709-489-7958</span>
				</div>
				
				<div class="member">
					<span class="member-name">Lisa Osbourne</span>
					<span class="member-title">Vice President</span>
					
					<span class="member-info phone">709-777-6066</span>
				</div>
				
				<div class="member">
					<span class="member-name">Thomas Walsh</span>
					<span class="member-title">Financial Secretary</span>
					
					<span class="member-info phone">709-778-0480</span>
				</div>
				
				<div class="member">
					<span class="member-name">Jim Bean</span>
					<span class="member-title">Treasurer</span>
					
					<span class="member-info phone">709-489-7958</span>
				</div>
				
				<div class="member">
					<span class="member-name">Tim Allen</span>
					<span class="member-title">Trustee</span>
					
					<span class="member-info phone">709-777-6066</span>
				</div>

			</div><!-- .member-grid -->
		
		</div><!-- .sw -->
	</section>
	
	<hr class="sw" />	
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>