<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">

			<div class="hgroup article-head">
				<h1 class="title">Login</h1>
				<span class="subtitle">Access the NAPE members-only area</span>
			</div><!-- .hgroup -->
		
			<div class="main-body">
				<div class="content">
				
					<div class="article-body">
					
						<p>
							Welcome to the NAPE members sign-in area. Please login to access the members-only area.
							If you are having problems logging on to the members only section, please contact NAPE at <a href="#">inquiries@nape.nf.ca</a>
						</p>
					
					</div><!-- .article-body -->
					
					<form action="/" class="body-form">
						<fieldset>
							
							<input type="text" name="login" placeholder="Login">
							<input type="password" name="password" placeholder="Password">
							
							<button type="submit" class="button fill">Login</button>
							
							<br />
							<br />
							
							<a href="#" class="inline">Forgot your username or password?</a>
							
						</fieldset>
					</form>
					
				</div><!-- .content -->
				<aside class="sidebar">
					
					<div class="mod">
						
						<?php include('inc/i-mod-the-latest.php'); ?>
						
					</div><!-- .mod -->
					
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-inline-search.php'); ?>
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-search-forms.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-affiliates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>