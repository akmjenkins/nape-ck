							<div class="mod-the-latest">
							
								<div class="hgroup mod-head">
									<h4 class="title">The Latest</h4>
									<span class="subtitle">The latest news, events and alert</span>
								</div><!-- .hgroup -->
								
								<div class="swiper-wrapper">
									<div class="swiper" data-next-arrow=".arrow-controls .next" data-prev-arrow=".arrow-controls .prev">
										<div class="swipe-item">
										
											<div class="grid ">
												<div class="col col-1">
													<a class="item dark-bg grid-item-with-tag" href="#">
													
														<span class="item-tag tag-event">Event</span>
														
														<time datetime="2014-03-12" class="i">
															<span class="day">12</span> Mar
														</time>
														
														<p>
															Paramedics Hold Demonstration About Staffing Levels and 'Red Alerts'
														</p>
														
														<span class="button">Read More</span>
														
													</a><!-- .item -->
												</div><!-- .col -->
											</div><!-- .grid -->

										</div><!-- .swipe-item -->
										<div class="swipe-item">
										
											<div class="grid ">
												<div class="col col-1">
													<a class="item dark-bg grid-item-with-tag" href="#">
													
														<span class="item-tag tag-event">Event</span>
														
														<time datetime="2014-03-12" class="i">
															<span class="day">12</span> Mar
														</time>
														
														<p>
															Paramedics Hold Demonstration About Staffing Levels and 'Red Alerts'
														</p>
														
														<span class="button">Read More</span>
														
													</a><!-- .item -->
												</div><!-- .col -->
											</div><!-- .grid -->

										</div><!-- .swipe-item -->
										<div class="swipe-item">
										
											<div class="grid ">
												<div class="col col-1">
													<a class="item dark-bg grid-item-with-tag" href="#">
													
														<span class="item-tag tag-event">Event</span>
														
														<time datetime="2014-03-12" class="i">
															<span class="day">12</span> Mar
														</time>
														
														<p>
															Paramedics Hold Demonstration About Staffing Levels and 'Red Alerts'
														</p>
														
														<span class="button">Read More</span>
														
													</a><!-- .item -->
												</div><!-- .col -->
											</div><!-- .grid -->

										</div><!-- .swipe-item -->
									</div><!-- .swiper -->
								</div><!-- .swiper-wrapper -->
								

								<div class="mod-controls">
									<div class="arrow-controls">
										<!-- these can also be "a" tags -->
										<button class="prev">Prev</button>
										<button class="next">Next</button>
									</div><!-- .arrow-controls -->
									
									<a href="#" class="button fill">View All</a>
									
								</div><!-- .mod-controls -->
							
							</div><!-- .mod-the-latest -->
