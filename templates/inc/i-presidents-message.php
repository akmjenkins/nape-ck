			<div class="news-items">
			
				<div class="grid collapse-1000">
				
					<div class="col-3 col">
						<div class="item">
							
							<h2>President's Message</h2>
							
							<div class="news-item">
								
								<div class="news-item-title">
								
									<time class="i" datetime="2014-03-24">
										<span class="day">24</span>
										Mar
									</time><!-- .i -->
								
									<div class="h5-style">
										President's Video Message Tentative Agreement Reached
									</div>
								
								</div><!-- .pmessage-title -->
								
								<div class="article-body">
								
									<p>
										The Provincial Government has announced a Memorandum of
										Agreement with NAPE and four other unions, that will result in
										changes to the Public Service Pension Plan (PSPP). This
										Agreement is intended to protect your pension plan now and
										into the future.
									</p>

									<p>
										The last actuarial valuation indicated the PSPP has a significant
										unfunded liability of 3.2 billion dollars. The unfunded liability and
										current service costs continue to grow. In 15 years the unfunded
										liability is projected to be 8.1 billion dollars. If not addressed,
										there is a strong potential that your retirement will be 
										jeopardized. Pension reform is inevitable.
									</p>
								
								</div><!-- .article-body -->
								
								<a href="#" class="button fill">View Transcript</a>
							
							</div><!-- .news-item -->
							
							<div class="news-items-controls">
							
								<div class="arrow-controls">
								
									<!-- these can also be "a" tags -->
									<button class="prev">Prev</button>
									<button class="next">Next</button>
									
								</div><!-- .controls -->
								
								<a href="#" class="button fill">Past Messages</a>
							</div><!-- .news-items-controls -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col-2-3 col">
						<div class="item">
							
							<div class="lazyyt" data-youtube-id="5LJZm7bpSdA" data-ratio="20:13"></div>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			
			</div><!-- .news-items -->
