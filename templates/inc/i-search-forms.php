			<div class="forms-grid">
		
				<div class="grid eqh pad40 collapse-950">
				
					<div class="col col-3">
						<div class="item">
							
							<div class="hgroup">
								<h4 class="title">Agreements</h4>
								<span class="subtitle">Find your agreement by entering your key terms below.</span>
							</div><!-- .hgroup -->
							
							<form action="/" class="single-form">
								<div class="fieldset">
									<input type="text" name="s" placeholder="Search agreements">
									<button type="submit" class="fa-search">&nbsp;</button>
								</div>
							</form><!-- .single-form -->
							
						</div><!-- .item -->
					</div><!-- .col -->

					<div class="col col-3">
						<div class="item">
							
							<div class="hgroup">
								<h4 class="title">Contact Your ERO</h4>
								<span class="subtitle">Get in touch with your NAPE Employee Relations Officer (ERO)</span>
							</div><!-- .hgroup -->
							
							<form action="/" class="single-form">
								<div class="fieldset">
									<input type="text" name="s" placeholder="Search by Name or NAPE local number">
									<button type="submit" class="fa-search">&nbsp;</button>
								</div>
							</form><!-- .single-form -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3">
						<div class="item">
							
							<div class="hgroup">
								<h4 class="title">Sign up for E-mail Updates</h4>
								<span class="subtitle">Up-to-date information about NAPE, its members, and what's happening</span>
							</div><!-- .hgroup -->
							
							<form action="/" class="single-form">
								<div class="fieldset">
									<input type="text" name="s" placeholder="Sign Up for Email Updates">
									<button type="submit" class="fa-paper-plane">&nbsp;</button>
								</div>
							</form><!-- .single-form -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
		
			</div><!-- .form-grid -->
