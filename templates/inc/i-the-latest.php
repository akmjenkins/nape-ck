			<div class="the-latest">
		
				<div class="hgroup">
					<h3 class="title">The Latest</h3>
					<span class="subtitle">Check out the latest information from NAPE</span>
				</div><!-- .hgroup -->
				
				<div class="grid eqh collapse-800 grid-tagged-items">
					<div class="col col-3">
						<a class="item dark-bg grid-item-with-tag" href="#">
						
							<span class="item-tag tag-news-release">News Release</span>
							
							<time datetime="2014-03-24" class="i">
								<span class="day">24</span> Mar
							</time>
							
							<p>
								NAPE Says Code Red Alert Increases Due to Shortage of Ambulances and Paramedic Staff.
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3">
						<a class="item dark-bg grid-item-with-tag" href="#">
						
							<span class="item-tag tag-alert">Alert</span>
							
							<time datetime="2014-03-03" class="i">
								<span class="day">03</span> Mar
							</time>
							
							<p>
								Eastern Health Paramedics to Hold Demonstration at Confederation Building Tomorrow.
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3">
						<a class="item dark-bg grid-item-with-tag" href="#">
						
							<span class="item-tag tag-event">Event</span>
							
							<time datetime="2014-03-12" class="i">
								<span class="day">12</span> Mar
							</time>
							
							<p>
								Paramedics Hold Demonstration About Staffing Levels and 'Red Alerts'
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
				
				<div class="center">
					<a href="#" class="button fill">View More</a>
				</div>
			
			</div><!-- .the-latest -->
